import '../snake_lib.dart';

class GameBoard {
  final int width;
  final int height;
  final tiles = <GameBoardTile>[];

  GameBoard({required this.width, required this.height});

  void initialize() {
    for (var y = 0; y < height; y++) {
      for (var x = 0; x < width; x++) {
        var state = TileState.background;
        if (y == 0 || y == height - 1) {
          state = TileState.border;
        } else if (x == 0 || x == width - 1) {
          state = TileState.border;
        }
        final tile = GameBoardTile(state, x: x, y: y);
        tiles.add(tile);
      }
    }
  }

  void beforeStepUpdate(Game game) {
    game.snake.beforeUpdate(this);
    game.food.beforeUpdate(this);
  }

  void afterStepUpdate(Game game) {
    game.snake.afterUpdate(this);
    game.food.afterUpdate(this);
  }

  void updateTile(int x, int y, TileState state) {
    final index = _toIndex(x, y);
    tiles[index].update(state);
  }

  GameBoardTile getTile(BoardPosition position) {
    final index = _toIndex(position.x, position.y);
    return tiles[index];
  }

  int _toIndex(int x, int y) {
    final result = x + width * y;
    return result;
  }
}
