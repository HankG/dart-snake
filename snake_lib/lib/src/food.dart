class Food {
  final int x;
  final int y;

  const Food(this.x, this.y);

  @override
  String toString() {
    return 'Food{x: $x, y: $y}';
  }
}
