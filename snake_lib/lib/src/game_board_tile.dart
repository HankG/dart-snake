import 'package:riverpod/riverpod.dart';
import 'package:snake_lib/src/board_position.dart';

enum TileState {
  border,
  background,
  food,
  head,
  body,
  ;
}

class GameBoardTile extends StateNotifier<TileState> {
  final BoardPosition position;

  GameBoardTile(super.state, {required int x, required int y})
      : position = BoardPosition(x: x, y: y);

  void update(TileState newState) {
    state = newState;
  }
}
