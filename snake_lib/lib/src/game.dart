import 'dart:async';
import 'dart:math';

import 'direction.dart';
import 'food.dart';
import 'snake.dart';

typedef GameEventHandler = void Function(Game game);

class Game {
  Timer? heartbeat;
  late final Snake snake;
  late Food _food;
  final int height;
  final int width;
  final gameSpeed = Duration(milliseconds: 200);

  final GameEventHandler beforeStep;
  final GameEventHandler afterStep;
  final GameEventHandler onGameOver;
  final GameEventHandler onStopRunning;

  var _score = 0;
  var _gameOver = false;

  Food get food => _food;

  bool get gameOver => _gameOver;

  int get score => _score;

  Game({
    required this.width,
    required this.height,
    required this.beforeStep,
    required this.afterStep,
    required this.onGameOver,
    required this.onStopRunning,
  }) {
    snake = Snake(x: width ~/ 2, y: height ~/ 2);
    _moveFoodRandomly();
  }

  void start() {
    heartbeat = Timer.periodic(gameSpeed, (_) {
      if (gameOver) {
        onGameOver(this);
      } else {
        update();
      }
    });
  }

  void stop() {
    onStopRunning(this);
    heartbeat?.cancel();
  }

  void update() {
    if (_gameOver) {
      onGameOver(this);
      return;
    }

    beforeStep(this);
    _wallCollisionUpdate();
    snake.step();
    _gameOver = _snakeHasCollision();
    _foodUpdate();
    afterStep(this);
  }

  void _foodUpdate() {
    if (snake.head.x == _food.x && snake.head.y == _food.y) {
      _moveFoodRandomly();
      _score++;
      snake.increaseLength();
    }
  }

  void _wallCollisionUpdate() {
    final head = snake.head;
    switch (snake.heading) {
      case Direction.up:
        if (head.y == 1) {
          snake.heading = head.x > width / 2 ? Direction.left : Direction.right;
        }
        break;
      case Direction.down:
        if (head.y == height - 2) {
          snake.heading = head.x > width / 2 ? Direction.left : Direction.right;
        }
        break;
      case Direction.left:
        if (head.x == 1) {
          snake.heading = head.y > height / 2 ? Direction.up : Direction.down;
        }
        break;
      case Direction.right:
        if (head.x == width - 2) {
          snake.heading = head.y > height / 2 ? Direction.up : Direction.down;
        }
        break;
    }
  }

  bool _snakeHasCollision() {
    final head = snake.head;
    for (int i = 1; i < snake.body.length; i++) {
      if (head == snake.body[i]) {
        return true;
      }
    }
    return false;
  }

  void _moveFoodRandomly() {
    var inSnake = true;
    while (inSnake) {
      final x = 1 + Random().nextInt(width - 3);
      final y = 1 + Random().nextInt(height - 3);
      _food = Food(x, y);
      inSnake = snake.body.where((b) => b.x == x && b.y == y).isNotEmpty;
    }
  }
}
