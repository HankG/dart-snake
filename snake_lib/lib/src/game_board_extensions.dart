import 'package:snake_lib/snake_lib.dart';

extension SnakeExtensions on Snake {
  void beforeUpdate(GameBoard gameBoard) {
    tail.clear(gameBoard);
    if (body.length > 1) {
      head.draw(gameBoard);
    }
  }

  void clear(GameBoard gameBoard) {
    body.forEach((s) => s.clear(gameBoard));
  }

  void afterUpdate(GameBoard gameBoard) {
    head.draw(gameBoard, state: TileState.head);
  }
}

extension FoodExtensions on Food {
  void beforeUpdate(GameBoard gameBoard) {
    draw(gameBoard, state: TileState.background);
  }

  void clear(GameBoard gameBoard) {
    draw(gameBoard, state: TileState.background);
  }

  void afterUpdate(GameBoard gameBoard) {
    draw(gameBoard);
  }

  void draw(GameBoard gameBoard, {TileState state = TileState.food}) {
    gameBoard.updateTile(x, y, state);
  }
}

extension SnakeSegmentExtensions on SnakeSegment {
  void draw(GameBoard gameBoard, {TileState state = TileState.body}) {
    gameBoard.updateTile(x, y, state);
  }

  void clear(GameBoard gameBoard) {
    draw(gameBoard, state: TileState.background);
  }
}
