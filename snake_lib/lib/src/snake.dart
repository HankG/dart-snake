import 'direction.dart';

class Snake {
  var heading = Direction.up;
  var body = <SnakeSegment>[];
  var _length = 1;

  int get length => _length;

  SnakeSegment get head => body.first;

  SnakeSegment get tail => body.last;

  Snake({required int x, required int y, this.heading = Direction.up}) {
    body.add(SnakeSegment(x: x, y: y));
  }

  void increaseLength() {
    _length++;
  }

  void step() {
    var head = body.first;
    var newX = head.x;
    var newY = head.y;
    switch (heading) {
      case Direction.up:
        newY--;
        break;
      case Direction.down:
        newY++;
        break;
      case Direction.left:
        newX--;
        break;
      case Direction.right:
        newX++;
        break;
    }
    if (body.length >= _length) {
      body.removeLast();
    }
    body.insert(0, SnakeSegment(x: newX, y: newY));
  }

  void changeHeading(Direction newHeading) {
    if (newHeading == Direction.up && heading != Direction.down) {
      heading = Direction.up;
    } else if (newHeading == Direction.down && heading != Direction.up) {
      heading = Direction.down;
    } else if (newHeading == Direction.left && heading != Direction.right) {
      heading = Direction.left;
    } else if (newHeading == Direction.right && heading != Direction.left) {
      heading = Direction.right;
    }
  }

  @override
  String toString() {
    return 'Snake{heading: $heading, length: $length, body: $body}';
  }
}

class SnakeSegment {
  final int x;
  final int y;

  const SnakeSegment({required this.x, required this.y});

  @override
  String toString() {
    return '[$x, $y]';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SnakeSegment &&
          runtimeType == other.runtimeType &&
          x == other.x &&
          y == other.y;

  @override
  int get hashCode => x.hashCode ^ y.hashCode;
}
