import 'package:flutter/material.dart';
import 'package:snake_lib/snake_lib.dart';

extension TileStateExtensions on TileState {
  Color get color {
    switch (this) {
      case TileState.border:
        return Colors.black;
      case TileState.background:
        return Colors.white10;
      case TileState.food:
        return Colors.yellow;
      case TileState.head:
        return Colors.green;
      case TileState.body:
        return Colors.lightGreen;
    }
  }
}
