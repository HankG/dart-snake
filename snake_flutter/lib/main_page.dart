import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:snake_lib/snake_lib.dart';

import 'arrow_controller.dart';
import 'game_board_widget.dart';
import 'riverpod_di.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  late FocusNode focusNode;
  late final Game game;
  static const width = 40;
  static const height = 40;
  final gameSpeed = const Duration(milliseconds: 200);
  var score = 0;

  @override
  void initState() {
    super.initState();
    focusNode = FocusNode();
    game = setupGame(gameBoard, width, height);
    game.start();
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: KeyboardListener(
          focusNode: focusNode,
          autofocus: true,
          onKeyEvent: keyboardHandler,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GameBoardWidget(
                  board: gameBoard,
                ),
                Text(
                  game.gameOver
                      ? 'Game Over, Final Score: ${game.score}'
                      : 'Score: ${game.score}',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                if (defaultTargetPlatform == TargetPlatform.iOS ||
                    defaultTargetPlatform == TargetPlatform.android) ...[
                  const SizedBox(
                    height: 5.0,
                  ),
                  ArrowController(snake: game.snake),
                ],
              ],
            ),
          ),
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Game setupGame(GameBoard gameBoard, int width, int height) {
    final game = Game(
        width: width,
        height: height,
        beforeStep: (game) {
          try {
            gameBoard.beforeStepUpdate(game);
          } on Exception catch (e, s) {
            print(e.toString());
            print(s.toString());
          }
        },
        afterStep: (game) {
          try {
            gameBoard.afterStepUpdate(game);
            if (game.score != score) {
              setState(() {
                score = game.score;
              });
            }
          } on Exception catch (e, s) {
            print(e.toString());
            print(s.toString());
          }
        },
        onGameOver: (game) {
          setState(() {});
        },
        onStopRunning: (game) {});

    return game;
  }

  void keyboardHandler(KeyEvent event) {
    final key = event.physicalKey;
    if (key == PhysicalKeyboardKey.arrowUp) {
      game.snake.changeHeading(Direction.up);
    } else if (key == PhysicalKeyboardKey.arrowDown) {
      game.snake.changeHeading(Direction.down);
    } else if (key == PhysicalKeyboardKey.arrowLeft) {
      game.snake.changeHeading(Direction.left);
    } else if (key == PhysicalKeyboardKey.arrowRight) {
      game.snake.changeHeading(Direction.right);
    }
  }
}
