import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:snake_flutter/flutter_tilestate_extensions.dart';
import 'package:snake_flutter/riverpod_di.dart';
import 'package:snake_lib/snake_lib.dart';

class GameBoardWidget extends StatelessWidget {
  final GameBoard board;

  const GameBoardWidget({super.key, required this.board});

  @override
  Widget build(BuildContext context) {
    const tileSize = 10.0;
    return SizedBox(
      width: board.width * tileSize,
      height: board.height * tileSize,
      child: GridView.count(
        crossAxisCount: board.width,
        children: board.tiles
            .map((t) => TileWidget(tileSize: tileSize, position: t.position))
            .toList(),
      ),
    );
  }
}

class TileWidget extends ConsumerWidget {
  final double tileSize;
  final BoardPosition position;

  const TileWidget({
    super.key,
    required this.tileSize,
    required this.position,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final tileState = ref.watch(gameTileProvider(position));
    return Container(
      width: tileSize,
      height: tileSize,
      color: tileState.color,
    );
  }
}
