import 'package:flutter/material.dart';
import 'package:snake_lib/snake_lib.dart';

class ArrowController extends StatelessWidget {
  final Snake snake;

  const ArrowController({super.key, required this.snake});

  @override
  Widget build(BuildContext context) {
    const buttonSize = 50.0;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
          iconSize: buttonSize,
          onPressed: () => snake.changeHeading(Direction.up),
          icon: const Icon(Icons.arrow_upward),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            IconButton(
              iconSize: buttonSize,
              onPressed: () => snake.changeHeading(Direction.left),
              icon: const Icon(Icons.arrow_back),
            ),
            const SizedBox(width: buttonSize,),
            IconButton(
              iconSize: buttonSize,
              onPressed: () => snake.changeHeading(Direction.right),
              icon: const Icon(Icons.arrow_forward),
            ),
          ],
        ),
        IconButton(
          iconSize: buttonSize,
          onPressed: () => snake.changeHeading(Direction.down),
          icon: const Icon(Icons.arrow_downward),
        ),
      ],
    );
  }
}
