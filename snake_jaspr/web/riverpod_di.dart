import 'package:jaspr_riverpod/jaspr_riverpod.dart';
import 'package:snake_lib/snake_lib.dart';

const width = 40;
const height = 40;

final gameBoard = GameBoard(width: width, height: height)..initialize();
final gameTileProvider =
    StateNotifierProvider.family<GameBoardTile, TileState, BoardPosition>(
        (ref, p) {
  return gameBoard.getTile(p);
});
