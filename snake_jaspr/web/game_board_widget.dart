import 'package:jaspr/jaspr.dart';
import 'package:jaspr_riverpod/jaspr_riverpod.dart';
import 'package:snake_lib/snake_lib.dart';

import 'jaspr_gameboard_extensions.dart';
import 'riverpod_di.dart';

class GameBoardWidget extends StatelessComponent {
  static final gridStyles = Styles.combine([
    Styles.grid(),
    Styles.box(width: Unit.pixels(400), height: Unit.pixels(400)),
    Styles.raw({
      'margin-left': 'auto',
      'margin-right': 'auto',
    }),
  ]);

  const GameBoardWidget({super.key});

  @override
  Iterable<Component> build(BuildContext context) sync* {
    const tileSize = 10.0;
    yield DomComponent(
      tag: 'div',
      styles: gridStyles,
      children: gameBoard.tiles
          .map((t) => TileWidget(
                tileSize: tileSize,
                position: t.position,
              ))
          .toList(),
    );
  }
}

class TileWidget extends StatelessComponent {
  final double tileSize;
  final BoardPosition position;

  const TileWidget({super.key, required this.tileSize, required this.position});

  @override
  Iterable<Component> build(BuildContext context) sync* {
    final state = context.watch(gameTileProvider(position));
    yield DomComponent(
        tag: 'div',
        styles: Styles.combine([
          position.styles,
          Styles.background(color: state.color),
          Styles.box(width: Unit.pixels(10), height: Unit.pixels(10)),
        ]));
  }
}
