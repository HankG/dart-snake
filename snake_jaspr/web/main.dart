import 'package:jaspr/browser.dart';
import 'package:jaspr_riverpod/jaspr_riverpod.dart';

import 'app.dart';

void main() {
  runApp(ProviderScope(child: App()));
}
