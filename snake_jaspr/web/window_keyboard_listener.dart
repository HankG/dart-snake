import 'dart:async';
import 'dart:html';

import 'package:jaspr/browser.dart';

typedef KeyboardEventHandler = Function(KeyboardEvent event);

class WindowKeyboardListener extends StatefulComponent {
  final KeyboardEventHandler? keyDownEventHandler;
  final KeyboardEventHandler? keyUpEventHandler;
  final KeyboardEventHandler? keyPressedEventHandler;

  const WindowKeyboardListener({
    super.key,
    this.keyDownEventHandler,
    this.keyUpEventHandler,
    this.keyPressedEventHandler,
  });

  @override
  WindowKeyboardListenerState createState() => WindowKeyboardListenerState();
}

class WindowKeyboardListenerState extends State<WindowKeyboardListener> {
  StreamSubscription<KeyboardEvent>? keyDownSubscription;
  StreamSubscription<KeyboardEvent>? keyUpSubscription;
  StreamSubscription<KeyboardEvent>? keyPressedSubscription;

  @override
  void initState() {
    super.initState();
    if (component.keyDownEventHandler != null) {
      keyDownSubscription =
          window.onKeyDown.listen(component.keyDownEventHandler);
    }

    if (component.keyUpEventHandler != null) {
      keyUpSubscription = window.onKeyUp.listen(component.keyUpEventHandler);
    }

    if (component.keyPressedEventHandler != null) {
      keyPressedSubscription =
          window.onKeyPress.listen(component.keyPressedEventHandler);
    }
  }

  @override
  void dispose() {
    keyDownSubscription?.cancel();
    keyUpSubscription?.cancel();
    keyPressedSubscription?.cancel();
    super.dispose();
  }

  @override
  Iterable<Component> build(BuildContext context) sync* {}
}
