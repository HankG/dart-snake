import 'package:jaspr/components.dart';
import 'package:snake_lib/snake_lib.dart';

extension TileStateExtensions on TileState {
  Color get color {
    switch (this) {
      case TileState.border:
        return Colors.black;
      case TileState.background:
        return Colors.white;
      case TileState.food:
        return Colors.gold;
      case TileState.head:
        return Colors.darkOliveGreen;
      case TileState.body:
        return Colors.green;
    }
  }
}

extension BoardPositionExtensions on BoardPosition {
  // use Styles.gridItem once fix is published to pub.dev
  Styles get stylesGI => Styles.gridItem(
          placement: GridPlacement(
        columnStart: LinePlacement(x + 1),
        columnEnd: LinePlacement(x + 1),
        rowStart: LinePlacement(y + 1),
        rowEnd: LinePlacement(y + 1),
      ));

  Styles get styles => Styles.raw({
        'grid-area': '${y + 1} / ${x + 1} / ${y + 1} / ${x + 1}',
      });
}
