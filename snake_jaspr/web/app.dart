import 'dart:html';

import 'package:jaspr/jaspr.dart';
import 'package:snake_lib/snake_lib.dart';

import 'game_board_widget.dart';
import 'riverpod_di.dart';
import 'window_keyboard_listener.dart';

class App extends StatefulComponent {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  late final Game game;
  var score = 0;

  @override
  void initState() {
    super.initState();
    game = setupGame(gameBoard, width, height);
    game.start();
  }

  @override
  void dispose() {
    game.stop();
    super.dispose();
  }

  @override
  Iterable<Component> build(BuildContext context) sync* {
    yield WindowKeyboardListener(
      keyDownEventHandler: keyboardhandler,
    );
    yield GameBoardWidget();
    yield DomComponent(
      id: 'score',
      tag: 'div',
      child: Text(
          game.gameOver ? 'Game Over, final score: $score' : 'Score: $score'),
    );
  }

  Game setupGame(GameBoard gameBoard, int width, int height) {
    final game = Game(
        width: width,
        height: height,
        beforeStep: (game) {
          try {
            gameBoard.beforeStepUpdate(game);
          } on Exception catch (e, s) {
            print(e.toString());
            print(s.toString());
          }
        },
        afterStep: (game) {
          try {
            gameBoard.afterStepUpdate(game);
            if (game.score != score) {
              setState(() {
                score = game.score;
              });
            }
          } on Exception catch (e, s) {
            print(e.toString());
            print(s.toString());
          }
        },
        onGameOver: (game) {
          setState(() {});
        },
        onStopRunning: (game) {});

    return game;
  }

  void keyboardhandler(KeyboardEvent event) {
    switch (event.keyCode) {
      case KeyCode.UP:
        game.snake.changeHeading(Direction.up);
        break;
      case KeyCode.DOWN:
        game.snake.changeHeading(Direction.down);
        break;
      case KeyCode.LEFT:
        game.snake.changeHeading(Direction.left);
        break;
      case KeyCode.RIGHT:
        game.snake.changeHeading(Direction.right);
        break;
      default:
        print('Unknown key: ${event.key}');
    }
  }
}
