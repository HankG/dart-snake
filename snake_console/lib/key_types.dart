import 'package:collection/collection.dart';

enum KeyTypes {
  up([27, 79, 65]),
  down([27, 79, 66]),
  left([27, 79, 68]),
  right([27, 79, 67]),
  backspace([127]),
  unknown([]),
  ;

  final List<int> codes;
  const KeyTypes(this.codes);

  static KeyTypes fromValue(List<int> value) {
    final eq = ListEquality();
    return values.firstWhere(
      (e) => eq.equals(e.codes, value),
      orElse: () => unknown,
    );
  }
}
