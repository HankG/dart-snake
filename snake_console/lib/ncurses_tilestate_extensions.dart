import 'package:snake_lib/snake_lib.dart';

extension TileStateExtensions on TileState {
  String get text {
    switch (this) {
      case TileState.border:
        return '';
      case TileState.background:
        return ' ';
      case TileState.food:
        return 'F';
      case TileState.head:
        return 'H';
      case TileState.body:
        return 'S';
    }
  }
}
