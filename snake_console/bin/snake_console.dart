import 'dart:async';
import 'dart:io';

import 'package:ncurses/ncurses.dart';
import 'package:snake_console/key_types.dart';
import 'package:snake_console/ncurses_tilestate_extensions.dart';
import 'package:snake_lib/snake_lib.dart';

final outputs = File('/tmp/log.txt').openWrite();

late final StreamSubscription<List<int>> stdinStreamSubscription;
late final GameBoard gameBoard;

Future<void> main(List<String> arguments) async {
  final screen = Screen();
  final height = lines - 3;
  final width = columns - 3;
  final game = setupGame(screen, width, height);

  stdinStreamSubscription = stdin.listen((keyValues) {
    final key = KeyTypes.fromValue(keyValues);
    final running = handleKeyStroke(game, key);
    if (!running) {
      game.stop();
    }
  });

  game.start();
}

void setupGameBoard(Window window, int width, int height) {
  gameBoard = GameBoard(width: width, height: height)..initialize();
  for (var i = 0; i < gameBoard.tiles.length; i++) {
    final tile = gameBoard.tiles[i];
    tile.addListener((state) {
      window.addString(state.text,
          at: Position(
            tile.position.x,
            tile.position.y,
          ));
    });
  }
}

Game setupGame(Screen screen, int width, int height) {
  final window = setupWindow(screen, width, height);
  setupGameBoard(window, width, height);
  outputs.writeln(
      'Width = $width, Height = $height, #elements = ${gameBoard.tiles.length}');
  return Game(
      width: width,
      height: height,
      beforeStep: (game) {
        try {
          gameBoard.beforeStepUpdate(game);
        } on Exception catch (e, s) {
          outputs.writeln(e.toString());
          outputs.writeln(s.toString());
        }
      },
      afterStep: (game) {
        try {
          gameBoard.afterStepUpdate(game);
          window.refresh();
        } on Exception catch (e, s) {
          outputs.writeln(e.toString());
          outputs.writeln(s.toString());
        }
      },
      onGameOver: (game) {
        window.addString(
          'Game over, score: ${game.score}.Press backspace to exit.',
          at: Position(0, 0),
        );
        window.redraw();
        window.refresh();
      },
      onStopRunning: (Game game) {
        stdinStreamSubscription.cancel();
        screen.endWin();
      });
}

Window setupWindow(Screen screen, int width, int height) {
  final window = Window.subWindow(
    screen.window,
    lines: height,
    cols: width,
    begin: Position(1, 1),
  );

  screen.echo = false;
  screen.raw = true;
  screen.cursorVisibility = CursorVisibility.invisible;
  window.keypad = true;
  window.allowScroll = true;
  window.border();
  window.refresh();
  return window;
}

bool handleKeyStroke(Game game, KeyTypes key) {
  var running = true;
  switch (key) {
    case KeyTypes.up:
      game.snake.changeHeading(Direction.up);
      break;
    case KeyTypes.down:
      game.snake.changeHeading(Direction.down);
      break;
    case KeyTypes.left:
      game.snake.changeHeading(Direction.left);
      break;
    case KeyTypes.right:
      game.snake.changeHeading(Direction.right);
      break;
    case KeyTypes.backspace:
      running = false;
      break;
    case KeyTypes.unknown:
      outputs.writeln('Unknown key values: $key');
      break;
  }

  return running;
}
