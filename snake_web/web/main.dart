import 'dart:html';

import 'package:snake_lib/snake_lib.dart';

import 'game_extensions.dart';

void main() {
  const width = 40;
  const height = 40;
  final game = setupGame(width, height);
  window.onKeyDown.listen((event) => keyboardHandler(event, game));
  game.start();
}

GameBoard setupGameBoard(int width, int height) {
  final gameBoard = GameBoard(width: width, height: height)..initialize();
  final gameBoardDomElements = querySelector('#game-board')!.children;
  for (final t in gameBoard.tiles) {
    final divElement = t.toDivElement();
    t.addListener((state) {
      divElement.className = state.styleName;
    });
    gameBoardDomElements.add(divElement);
  }
  return gameBoard;
}

Game setupGame(int width, int height) {
  final gameBoard = setupGameBoard(width, height);
  final scoreElement = querySelector('#score')!;
  scoreElement.text = 'Score: 0';
  final game = Game(
      width: width,
      height: height,
      beforeStep: (game) {
        try {
          gameBoard.beforeStepUpdate(game);
        } on Exception catch (e, s) {
          print(e.toString());
          print(s.toString());
        }
      },
      afterStep: (game) {
        try {
          gameBoard.afterStepUpdate(game);
          scoreElement.text = 'Score: ${game.score}';
        } on Exception catch (e, s) {
          print(e.toString());
          print(s.toString());
        }
      },
      onGameOver: (game) {
        scoreElement.innerHtml = 'GAME OVER! <br>Final Score: ${game.score}';
      },
      onStopRunning: (Game game) {});

  return game;
}

void keyboardHandler(KeyboardEvent event, Game game) {
  switch (event.keyCode) {
    case KeyCode.UP:
      game.snake.changeHeading(Direction.up);
      break;
    case KeyCode.DOWN:
      game.snake.changeHeading(Direction.down);
      break;
    case KeyCode.LEFT:
      game.snake.changeHeading(Direction.left);
      break;
    case KeyCode.RIGHT:
      game.snake.changeHeading(Direction.right);
      break;
    default:
      print('Unknown key: ${event.key}');
  }
}
