import 'dart:html';

import 'package:snake_lib/snake_lib.dart';

extension TileStateExtensions on TileState {
  String get styleName => 'tile-$name';
}

extension GameBoardTileExtension on GameBoardTile {
  DivElement toDivElement() {
    final x = position.x;
    final y = position.y;
    final divElement = DivElement();
    divElement.className = state.styleName;
    divElement.style.gridRowStart = '${y + 1}';
    divElement.style.gridRowEnd = '${y + 1}';
    divElement.style.gridColumnStart = '${x + 1}';
    divElement.style.gridColumnEnd = '${x + 1}';
    return divElement;
  }
}
